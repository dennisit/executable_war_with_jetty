本项目以及主要源代码参考github上的https://github.com/todylu/example_executable_war_with_jetty
原链接中使用的是jetty7,我主要是改了一下JDK和jetty版本，同时解决了在此过程中遇到的一些包冲突问题。

本项目目的是基于maven建立一个可以自运行的基于jetty的war包。
生成的war包同时也可以放到传统的tomcat目录下运行。
========================================
生成的war包在windows平台下,cmd中输入java -jar -Dport=10080 jettyExam.war运行时，会使用默认的JRE环境，访问JSP文件会出现类似问题：
org.apache.jasper.JasperException: PWC6345: There is an error in invoking javac. A full JDK (not just JRE) is required
故在windows下应该指定使用JDK下的java.exe运行：%JAVA_HOME%\bin\java -jar -Dport=10080 jettyExam.war (假设环境变量JAVA_HOME未设置，则写JDK全安装路径)
Or specify JVM:%JAVA_HOME%\bin\java -jar -Xms512m -Xmx1024m -XX:PermSize=256M -XX:MaxPermSize=256M -Dport=10080 jettyExam.war
=============================================================================================================
the executable war with embedded jetty
=====
The project is created by __Eclipse Luna__  
Project name : __jettyExam__  
Environment requirment : __Maven 3.2.3__  

The main function is defined in `"jettyExam/src/main/java/Runner.java"`. It is responsible to create a Jetty Server.  
The servlet app is in `"jettyExam/src/main/java/hello/app/HelloWorldServlet.java"`.  
The map which is to communicate url and servlet app is defined in `"jettyExam/src/main/webapp/WEB-INF/web.xml"`.  

The example dosen't support JSP, because some jar file is not included.  
The main effort is to maintain `"jettyExam/pom.xml"`:  
In order to source code is compiled successfully, the dependency is key point.  

                <dependency>  
                  <groupId>org.eclipse.jetty</groupId>  
                  <artifactId>jetty-server</artifactId>  
                  <version>${jettyVersion}</version>  
                  <scope>provided</scope>  
                </dependency>  
                <dependency>  
                  <groupId>org.eclipse.jetty</groupId>  
                  <artifactId>jetty-webapp</artifactId>  
                  <version>${jettyVersion}</version>  
                  <scope>provided</scope>  
                </dependency>  
 
Meanwhile, `"provided"` can avoid these jar files to be packaged into war file.    

If JSP need be supported, the following dependency should be contained:    

              <dependency>
                <groupId>org.eclipse.jetty</groupId>
                <artifactId>jetty-jsp</artifactId>
                <version>${jettyVersion}</version>
                <scope>provided</scope>              
            </dependency>
The war file would increase 800KB size, if JSP dependency is contained.     
So it suggest to comment the JSP dependency.    

In addition, some plugins are essential.    
__maven-war-plugin__ will define "main" class in _MANIFEST.MF_.    
__maven-antrun-plugin__ will move the "main" class to the root path of war file.  
__maven-dependency-plugin__ will decompress the dependency jar files.  

After perform "run"->"run as"->"maven install" in Eclipse, the `jettyExam.war` can be found in the target fold.   
now we can run `java -jar -Dport=10080 jettyExam.war` and check the result via browsing `http://localhost:10080/` and `http://localhost:10080/helloworld`    

reference:    
[Step by step: Executable WAR files](http://internna.blogspot.com/2011/08/step-by-step-executable-war-files.html)    
Pay attention, there is wrong in the article.